const assert = require('assert-plus')
const http = require('http')
const App = require('../src/app.js')
const Server = require('../src/server.js')
const config = require('../src/config.js')

describe('server', () => {
  let fake_server, fake_app

  const some_port = 3333
  const some_host = '7.7.7.7'

  beforeEach(() => {
    fake_server = jasmine.createSpyObj('server', ['listen', 'address'])
    fake_app = jasmine.createSpy('app')

    spyOn(http, 'createServer').and.returnValue(fake_server)
    spyOn(App, 'initialize').and.returnValue(fake_app)
    spyOn(config, 'getServerHost').and.returnValue(some_host)
    spyOn(config, 'getServerPort').and.returnValue(some_port)
  })

  it('is initialized at the correct address', () => {
    const preventCallback = (..._args) => {}
    fake_server.listen.and.callFake(preventCallback)

    const server = Server.initialize()

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_port, some_host, jasmine.any(Function)
    )
  })

  it('logs the address to the console', () => {
    const fake_server_address = jasmine.createSpy()

    fake_server.address.and.returnValue(fake_server_address)

    fake_server.listen.and.callFake((...args) => {
      const [cb,] = args.slice().reverse()

      assert.func(cb, 'cb')

      spyOn(console, 'log')

      cb()

      expect(console.log).toHaveBeenCalledWith('Server listening at address: %j',
        fake_server_address)
    })

    const server = Server.initialize()

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_port, some_host, jasmine.any(Function)
    )
  })
})

