const assert = require('assert-plus')

exports.requireNoCache = (ext_require, src_path) => {
  assert.func(ext_require, 'ext_require')
  assert.string(src_path, 'src_path')

  const lib = ext_require(src_path)

  delete ext_require.cache[ext_require.resolve(src_path)]

  return lib
}

exports.basicAuthCredentials = ({ user, password } = {}, opts = {}) => {
  assert.string(user, 'user')
  assert.string(password, 'password')
  assert.object(opts, 'opts')

  const type = opts.lowercase_type ? 'basic' : 'Basic'

  const raw_credentials = [user, password].join(':')
  const base64_credentials = Buffer.from(raw_credentials).toString('base64')

  return [type, base64_credentials].join(' ')
}

