const assert = require('assert-plus')
const Express = require('express')
const supertest = require('supertest')
const { basicAuthCredentials } = require('../support/helpers.js')
const { basicAuth } = require('../../src/middleware/basic_auth.js')

describe('basic auth middleware', () => {
  const user = 'frank'
  const password = '1234abcd'

  const invalid_user = 'jimmy'
  const invalid_password = 'hatwa9vgra'

  beforeAll(() => {
    assert.notStrictEqual(user, invalid_user)
    assert.notStrictEqual(password, invalid_password)
  })

  const makeApp = basicAuthMiddleware => Express()
    .use(basicAuthMiddleware)
    .use((req, res) => res.sendStatus(200))
    .use((err, req, res, next) => { console.error(err); next(err) })

  describe('when no Authorization header is provided', () => {
    it('does not let the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      await supertest(app)
        .get('/')
        .expect(401)
    })
  })

  describe('when the Authorization header is not for basic auth', () => {
    it('does not let the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      await supertest(app)
        .get('/')
        .set('Authorization', 'Bearer b78t57at45ofg4w')
        .expect(401)
    })
  })

  describe('when the credentials do not match', () => {
    it('does not allow the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      const basic_auth = basicAuthCredentials({
        user: invalid_user,
        password: invalid_password
      })

      await supertest(app)
        .get('/')
        .set('Authorization', basic_auth)
        .expect(401)
    })
  })

  describe('when the username does not match', () => {
    it('does not allow the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      const basic_auth = basicAuthCredentials({
        user: invalid_user,
        password
      })

      await supertest(app)
        .get('/')
        .set('Authorization', basic_auth)
        .expect(401)
    })
  })

  describe('when the password does not match', () => {
    it('does not allow the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      const basic_auth = basicAuthCredentials({
        user,
        password: invalid_password
      })

      await supertest(app)
        .get('/')
        .set('Authorization', basic_auth)
        .expect(401)
    })
  })

  describe('when the credentials match', () => {
    it('allows the request through', async () => {
      const app = makeApp(basicAuth({ user, password }))

      await supertest(app)
        .get('/')
        .set('Authorization', basicAuthCredentials({ user, password }))
        .expect(200)
    })
  })

  describe('when the authorization header is malformed', () => {
    describe('when missing authorization type', () => {
      it('does not let the request through', async () => {
        const app = makeApp(basicAuth({ user, password }))

        await supertest(app)
          .get('/')
          .set('Authorization', 'foobar')
          .expect(401)
      })
    })

    describe('when the ":" separator is missing', () => {
      it('does not let the request through', async () => {
        const app = makeApp(basicAuth({ user, password }))

        await supertest(app)
          .get('/')
          .set('Authorization', 'Basic foobarbaz')
          .expect(401)
      })
    })

    describe('when there are multple instances of the ":" separator', () => {
      it('does not let the request through', async () => {
        const app = makeApp(basicAuth({ user, password }))

        const malformed_credentials = Buffer
          .from(`${user}:${password}:${password}`)
          .toString('base64')          

        await supertest(app)
          .get('/')
          .set('Authorization', basic(malformed_credentials))
          .expect(401)
      })
    })
  })

  describe('header type', () => {
    it('must be case-insensitive', async () => {
      const app = makeApp(basicAuth({ user, password }))

      const basic_auth = basicAuthCredentials({ user, password },
        { lowercase_type: true })

      await supertest(app)
        .get('/')
        .set('Authorization', basic_auth)
        .expect(200)
    })
  })

  const basic = (credentials = '') => ['Basic', credentials].join(' ')
})

