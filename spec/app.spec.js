const supertest = require('supertest')
const App = require('../src/app.js')
const { basicAuthCredentials } = require('./support/helpers.js')

describe('App', () => {
  let original_env

  const FAKE_BASIC_AUTH_USER = 'joe'
  const FAKE_BASIC_AUTH_PASSWORD = 'heyjoe'

  beforeEach(() => {
    original_env = process.env

    process.env = {
      ...process.env,
      BASIC_AUTH_USER: FAKE_BASIC_AUTH_USER,
      BASIC_AUTH_PASSWORD: FAKE_BASIC_AUTH_PASSWORD
    }
  })

  afterEach(() => {
    process.env = original_env
  })

  describe('.initialize', () => {
    it('initializes the application', () => {
      const app = App.initialize()
      expect(app).toEqual(jasmine.any(Function))
    })
  })

  describe('secured endpoint', () => {
    const requestSecureEndpoint = (app = App.initialize()) =>
      supertest(app).get('/ping')

    describe('when requested without credentials', () => {
      it('is not reached', async () => {
        await requestSecureEndpoint()
          .expect(401)
      })
    })

    describe('when requested with valid credentials', () => {
      it('responds', async () => {
        const basic_auth = basicAuthCredentials({
          user: FAKE_BASIC_AUTH_USER,
          password: FAKE_BASIC_AUTH_PASSWORD
        })

        await requestSecureEndpoint()
          .set('authorization', basic_auth)
          .expect(200, { message: 'pong' })
      })
    })
  })
})

