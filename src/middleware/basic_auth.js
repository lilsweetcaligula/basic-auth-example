const assert = require('assert-plus')

exports.basicAuth = params => {
  assert.string(params.user, 'params.user')
  assert.string(params.password, 'params.password')

  const { user, password } = params
  
  return (req, res, next) => {
    const authorization = req.headers['authorization']

    if (typeof authorization !== 'string') {
      return res.sendStatus(401)
    }

    const header_parts = authorization.split(' ')  

    if (header_parts.length !== 2) {
      return res.sendStatus(401)
    }

    const [type, credentials] = header_parts

    if (type.toLowerCase() !== 'basic') {
      return res.sendStatus(401)
    }

    const raw_credentials = Buffer.from(credentials, 'base64').toString()

    if (!raw_credentials.includes(':')) {
      return res.sendStatus(401)
    }

    const credentials_parts = raw_credentials.split(':')

    if (credentials_parts.length !== 2) {
      return res.sendStatus(401)
    }

    const [req_user, req_password] = credentials_parts

    if (req_user === user && req_password === password) {
      return next()
    }

    return res.sendStatus(401)
  }
}

