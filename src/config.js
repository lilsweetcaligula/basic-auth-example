const assert = require('assert-plus')
const dotenv = require('dotenv-safe')

dotenv.config()

const config = {
  getServerPort: () => config.fromEnv('SERVER_PORT'),

  getServerHost: () => config.fromEnv('SERVER_HOST'),

  getBasicAuthUser: () => config.fromEnv('BASIC_AUTH_USER'),

  getBasicAuthPassword: () => config.fromEnv('BASIC_AUTH_PASSWORD'),

  fromEnv: env_var => {
    assert.string(env_var, 'env_var')

    if (env_var in process.env) {
      return process.env[env_var]
    }

    throw new Error('process.env["' + env_var + '"] is not defined')
  }
}

module.exports = config
