const http = require('http')
const App = require('./app.js')
const config = require('./config.js')

exports.initialize = () => {
  const app = App.initialize()

  const host = config.getServerHost()
  const port = config.getServerPort()

  const server = http.createServer(app)

  server.listen(port, host, () => {
    console.log('Server listening at address: %j', server.address())
  })

  return server
}

