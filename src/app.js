const Express = require('express')
const { getBasicAuthUser, getBasicAuthPassword } = require('./config.js')
const { handleAppError } = require('./middleware/handle_app_error.js')
const { basicAuth } = require('./middleware/basic_auth.js')

exports.initialize = () => {
  const app = Express()

  app.get(
    '/ping',

    basicAuth({
      user: getBasicAuthUser(),
      password: getBasicAuthPassword()
    }),

    (req, res) => res.json({ message: 'pong' })
  )

  app.use(handleAppError())

  return app
}
